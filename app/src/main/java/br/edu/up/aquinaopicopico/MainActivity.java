package br.edu.up.aquinaopicopico;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;

public class MainActivity extends AppCompatActivity
    implements SensorEventListener,
    MediaPlayer.OnCompletionListener {

  private SensorManager sm;
  private Sensor sProximidade;
  private MediaPlayer mediaPlayer;
  private Camera camera;
  private SurfaceTexture surfaceTexture;
  private boolean isPiscando;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    sm = (SensorManager) getSystemService(SENSOR_SERVICE);
    sProximidade = sm.getDefaultSensor(Sensor.TYPE_PROXIMITY);

    try {
      camera = Camera.open();
      surfaceTexture = new SurfaceTexture(0);
      camera.setPreviewTexture(surfaceTexture);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    sm.unregisterListener(this);
    if (mediaPlayer != null) {
      mediaPlayer.pause();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    sm = null;
    if (mediaPlayer != null){
      mediaPlayer.release();
    }
    mediaPlayer = null;
    if (camera != null) {
      camera.release();
    }
    camera = null;
  }

  @Override
  protected void onResume() {
    super.onResume();
    sm.registerListener(this, sProximidade, SensorManager.SENSOR_DELAY_FASTEST);
    if (mediaPlayer != null) {
      mediaPlayer.start();
    }
  }

  @Override
  public void onSensorChanged(SensorEvent event) {

    if (event.values.length > 0) {
      float proximidade = event.values[0];
      if (proximidade < 5) {

        if(!isPiscando){
          isPiscando = true;
          new Thread(new Runnable() {
            @Override
            public void run() {
              for (int i = 0; i < 10; i++){
                try {
                  if (i % 2 == 0){
                    ligarFlash();
                  } else {
                    desligarFlash();
                  }
                  Thread.sleep(750);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
              isPiscando = false;
            }
          }).start();
        }

        if (mediaPlayer == null) {
          mediaPlayer = MediaPlayer.create(this, R.raw.alarme);
          mediaPlayer.setOnCompletionListener(this);
          mediaPlayer.start();
        }
      }
    }
  }

  private void ligarFlash(){
    Camera.Parameters p = camera.getParameters();
    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
    camera.setParameters(p);
    camera.startPreview();
    isPiscando = true;
  }

  private void desligarFlash(){
    Camera.Parameters p = camera.getParameters();
    p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
    camera.setParameters(p);
    camera.stopPreview();
    isPiscando = false;
  }

  @Override
  public void onCompletion(MediaPlayer mp) {
    mediaPlayer = null;
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
    //Não implementado.
  }
}
